<?php

$donnees = array(1,2,2,2,3,7,8,12);

$filtre = array(2);

$resultat = array();

foreach($donnees as $une_valeur){
    if( ! in_array($une_valeur, $filtre) ){
        $resultat[] = $une_valeur;
    }
}

echo implode(", ", $resultat);