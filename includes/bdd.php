

<form action="index.php" method="POST">
    <input type="hidden" name="page" value="bdd" />

    <div class="form-group">
        <label for="email">Email</label>
        <input class="form-control" type="email" name="email" value="" placeholder="saisissez ici votre adresse email" />
    </div>
    <div class="form-group">
        <label for="password">Mot de passe</label>
        <input class="form-control" type="password" name="password" value="" placeholder="saisissez ici votre mot de passe" />
    </div>

    <button type="submit" class="btn btn-primary">Connexion</button>
</form>



<?php

if ( isset($_SESSION['user']['id']) ){

    $bdd = dbconnect();

    $sql='
        SELECT * 
        FROM articles
        WHERE author_id = :uid
    ';
    
    $sth = $bdd->prepare($sql);

    $sth->execute(array(
        'uid' => $_SESSION['user']['id']
    ));

    $tous_mes_articles = $sth->fetchAll();

    if( count($tous_mes_articles) > 0 ){
        echo "<h2>La liste des articles que j'ai écrit :</h2>";
    }

    foreach( $tous_mes_articles as $article){
        echo '<h3>' . $article['title'] . '</h3>';

        echo strip_tags(substr($article['content'], 0, 500));
    }
}




?>