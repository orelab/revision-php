
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.1/assets/img/favicons/favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/starter-template/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">
  </head>

  <body>




    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="#">Orel</a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a class="nav-link" href="index.php">Accueil</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?page=csv">CSV</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?page=blog">Blog</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?page=bdd">Connexion</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?page=contact">Contact</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?page=config">Configuration</a></li>
        </ul>


          <?php if( isset($_SESSION['user']) ): ?>

          <p>Bienvenue, <?=$_SESSION['user']['firstname'] ?></p>

          <form class="form-inline" action="/" method="get">
            <input type="hidden" name="page" value="<?=$_REQUEST['page']?>" />
            <input type="hidden" name="action" value="disconnect" />
            <button class="btn btn-outline-success">déconnexion</button>
          </form>

          <?php endif ?>

      </div>

    </nav>



    <main role="main" class="container">
        <h1><?= $titre ?></h1>

