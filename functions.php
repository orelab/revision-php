<?php

require '../configuration.php';


function afficher_entete($titre){
    require '../parts/entete.php';
}

function afficher_pieddepage(){
    require '../parts/pieddepage.html';
}


function charger_page($nom_page){
// echo ">$nom_page<";

    switch($nom_page){
        case "csv": require "../includes/csv.php"; break;
        case "blog": require "../includes/blog.php"; break;
        case "contact": require "../includes/contact.php"; break;
        case "config": require "../includes/configuration.php"; break;
        case "bdd": require "../includes/bdd.php"; break;
        default: require "../includes/accueil.php"; break;
    }
}

function charger($fichier){
    $csv = array_map('str_getcsv', file($fichier));
    // print_r($csv);
    return $csv;
}

function dbconnect(){
    global $dbname, $dbuser, $dbpassword;

    try {
        return new PDO('mysql:dbname='.$dbname.';host=localhost', $dbuser, $dbpassword);
   } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
   }
}


function authenticate( $email, $password ){

    // Récupérer les infos de l'utilisateur dont l'email correspond

    $bdd = dbconnect();

    $sql = 'SELECT * FROM authors WHERE email= :email';

    $sth = $bdd->prepare($sql);
    $sth->execute(array(':email' => $email));
    $resultat = $sth->fetchAll();

    // Commencer par vérifier qu'un utilisateur possède cette adresse email

    if( count($resultat) > 0){

        // Si c'est le cas, le mot de passe proposé correspond-il au hash stocké en BDD ?

        if(password_verify($password, $resultat[0]['password'])){
            $_SESSION['user'] = $resultat[0];
            return true; // email ok, password ok
        }
        else return false; // email ok, password missmatch
    }
    else return false; // email not found
}
