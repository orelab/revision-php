<?php
session_start();

require '../functions.php';



/*

    Quand on clique sur le bouton déconnexion, on supprime
    simplement toutes les infos stockées dans la session.
    Voir le fichier entete.php pour plus d'infos.

    Notez que la session est en cours de destruction, mais 
    existera encore au chargement de cette page (le bouton 
    'déconnexion' sera donc encore visible). En rechageant
    la page, on résoud le problème ;)

*/
if( $_GET['action'] === 'disconnect' ){
    session_destroy();
    header('Location: /');
}



 
/*

    Les données envoyées depuis le formulaire de connexion
    sont traitées ici. Si l'authentification réussit (voir
    la fonction authenticate() pour plus d'infos), on ajoute
    dans la $_SESSION une clé 'user' qui contient toutes les
    infos de la personnes connectée.

*/
if( isset($_POST['email']) && isset($_POST['password']) ){

   if( authenticate($_POST['email'], $_POST['password']) ){
       echo '<p>Je suis identifié en tant que ' . $_POST['email'] . ' </p>';
   } else {
       echo '<p>Identification échouée avec l\'email ' . $_POST['email'] . ' </p>';
   }
}




/*

    Ce petit bout de script pour créer en dur dans la base de données
    des clés de hachage compatibles avec PHP password_hash() et
    password_verify()

    https://stackoverflow.com/questions/40358263/which-mysql-fuction-is-equal-to-php-password-hash

    Pour l'utiliser, visiter l'url suivante :
    http://0.0.0.0:8000/?genpass=LEMOTDEPASSE

*/
if($_GET['genpass']){
    die( password_hash($_GET['genpass'], PASSWORD_BCRYPT) );
}



$page = $_REQUEST['page'];


afficher_entete($page);

charger_page($page);

afficher_pieddepage();
