
## Notice d'installation

Pour installer et configurer le projet, faire ceci :

```
git clone git@gitlab.com:orelab/revision-php.git
cd revision-php/
cp configuration.sample.php configuration.php
# modifier les valeurs du fichier configuration.php
```


# Lancer le serveur

Pour lancer le serveur, vous pouvez utiliser le mini 
serveur inclus avec PHP pour servir le contenu de public/ 
(le script index.php est le point d'entrée unique qui
génère toutes les pages)

```
cd public/
php -S 0.0.0.0:8000
```

